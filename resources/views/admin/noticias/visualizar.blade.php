@extends('layouts.admin')

@section('titulo','Área Administrativa')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Vizualizar Notícia</h2>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table table-striped table-condensed">
                <td>
                    <th width="150">ID</th>
                    <td>1</td>
                </td>
                <td>
                    <th width="150">Título</th>
                    <td>Flamengo ganhou do corinthias</td>
                </td>
                <td>
                    <th width="150">Descrição</th>
                    <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iste a quam nobis, minus assumenda sint cum perspiciatis aperiam adipisci quos temporibus eligendi recusandae praesentium alias harum error minima repellat autem.</td>
                </td>
            </table>
        </div>
    </div>
    <div class="form-group row">
        <button type="submit" class="btn badge-danger">Editar Notícia</button>
        <a href="#" class="btn btn-secondary">Cancelar</a>
    </div>
</div>
    
@endsection
<form action="" method="POST">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="titulo">Nome</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="titulo" name="titulo" value="">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">E-Mail</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="subtitulo" name="subtitulo" value="">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">Password</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="username" name="username" value="">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="subtitulo">Perfil</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="subtitulo" name="subtitulo" value="">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="descricao">Foto</label>
            <div class="col-sm-10">
                <img class="img-fluid" src="https://via.placeholder.com/250x240">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="status">Permissão</label>
            <div class="col-sm-5">
                <select class="form-control" id="status" name="status">
                    <option value="valor1">Colaborador</option>
                    <option value="valor2">Editor</option>
                    <option value="valor3">Administrador</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label text-right font-weight-bold" for="status">Status</label>
            <div class="col-sm-5">
                <select class="form-control" id="status" name="status">
                    <option value="0">Ativo</option>
                    <option value="1">Inativo</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class=" offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-danger">Salvar</button>
                <a href="#" class="btn btn-secondary">Cancelar</a>
            </div>   
        </div>
    </form>
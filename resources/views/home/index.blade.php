@extends('layouts.app')

@section('titulo','Home')

@section('conteudo')

<div class="container">
    <article class="row">    
            <div class="col-md-4">
                <img class="img-fluid" src="https://via.placeholder.com/400x250">
            </div>
            <div class="col-md-8">
                <h2>Noticia Destaque</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio et nisi sed, sint fugiat soluta unde
                    numquam animi ullam praesentium accusantium eveniet perferendis, quas ipsa esse neque magnam
                    obcaecati quasi!</p>
            </div>      
    </article>
    <div class="row">

        @for($i = 1; $i <= 3; $i++) 
        
         <div class="col-md-4 mt-5">
            <article class="card">
                <a href="#">
                    <img class="img-fluid" src="https://via.placeholder.com/500x250">
                </a>

                <div class="card-body">
                    <h2 class="card-title">
                        <a href="#">Titulo Noticia</a>
                    </h2>
                    <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Harum ad praesentium
                        quibusdam facilis modi, voluptates tempore sunt commodi, culpa qui quia magni odit adipisci non
                        repellendus. Ex corporis sapiente delectus.</p>
                </div>
                <div class="card-footer">
                    30/04/2019
                </div>
            </article>
        </div>

    @endfor

</div>
</div>

@endsection
